# IMarket

## Instalando o projeto

  Aṕos clonar o projeto através do comando:  

    $ git clone https://gitlab.com/rogeriojunior/EP3_OO.git

  Dentro da pasta raiz, deve-se executar os seguintes comandos:  
    
    $ bundle install
    $ bundle exec rake db:create
    $ bundle exec rake db:drop
    $ bundle exec rake db:migrate    
    $ rails server  

## Acessando o Site

  Para acessar o site basta acessar o link

    localhost:3000

## O Site

  O site consiste em um sistema de venda e compra de produtos culinários. Para comprar/postar o usuário deve criar uma conta e estar logado. 
